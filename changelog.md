### 1.1.0.0

* Add `openWith` and `createSqlitePoolWith`.
* `createSqlitePool` will now set the `foreign_keys` pragma and the `busy_timeout` pragma to 30000ms.

### 1.0.1.0

* Fix compilation with direct-sqlite 2.3.29.

### 1.0.0.0

* Bump resource-pool bounds to >=0.4 && <5.

### 0.2.0.1

* Fix: finalize prepared statements even when errors are encountered.

### 0.2.0.0

* Rework the API
  * Introduce the `SQLite a` type instead of `Database -> IO a` for `run`, `runWith`, `withDB`, `migrate`, and so on.
  * Introduce `withPool` and export `liftIO`, `fromString` and `void`.
  * Depend on `unliftio-core` and `mtl`.
* Implement nested transactions via savepoints.
  * `asTransaction` renamed to `transaction`.
  * `cancelTransaction` split to `rollback` and `rollbackAll`.
